using UnityEngine;
namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Renderer)]
	[Tooltip("add line renderer")]
	public class AddLine : FsmStateAction
	{   
		
		
		
		
		[RequiredField]
		[Tooltip("The Game Object to add the Component to.")]
		public FsmOwnerDefault gameObject;
		public FsmFloat lineWidthStart;	
		public FsmFloat lineWidthEnd;
		public Transform[] linkedObj;
		
		//public FsmFloat lineWidthStart;
		
		
		public Color lineColorStart;
		public Color lineColorEnd;
		
		public override void Reset ()
		{
//			Debug.Log("reset");
		}
		
		public override void OnEnter ()
		{
			
			
		}
		
		public override void OnUpdate ()
		{
//			Debug.Log("On OnUpdate");
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			LineRenderer lr = go.GetComponent<LineRenderer>();
			lr.enabled = true;
			lr.SetColors(lineColorStart, lineColorEnd);
			lr.SetWidth(lineWidthStart.Value, lineWidthEnd.Value);
			if(lr != null)
			{
				lr.SetVertexCount(linkedObj.Length);
				for(int i = 0; i<linkedObj.Length; i++)
				{
					Transform t = linkedObj[i];
					lr.SetPosition(i, t.position);
				}
			}
		}
		
		
		public override void OnExit ()
		{
//			Debug.Log("On exit");
			GameObject go = Fsm.GetOwnerDefaultTarget(gameObject);
			LineRenderer lr = go.GetComponent<LineRenderer>();
			if(lr != null)
			{
				lr.enabled = false;
			}
		}
		
		
	}
	
}

