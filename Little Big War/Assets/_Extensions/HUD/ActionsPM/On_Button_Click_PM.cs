//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using HutongGames.PlayMaker;
using CrenoLab.UniCAE.Util;

[ActionCategory("UniGameKit_HUD")]
public class On_Button_Click_PM : FsmStateAction
{
    public FsmOwnerDefault gameObject;
    public FsmEvent clickEvent;
    
	// Code that runs on entering the state.
	public override void OnEnter()
	{
        var go = Fsm.GetOwnerDefaultTarget(gameObject);
        if (null == go)
            return;

        UIButton btn = go.GetComponent<UIButton>();
        if (null == btn)
            return;

        NGUIClickUnitObj cpt = btn.gameObject.AddComponent<NGUIClickUnitObj>();
        cpt.OnClickCallBack = OnClick;
	}

	// Code that runs every frame.
	public override void OnUpdate()
	{
        
	}

	public override void OnFixedUpdate()
	{
		
	}

	public override void OnLateUpdate()
	{
		
	}

	// Code that runs when exiting the state.
	public override void OnExit()
	{
		
	}

	// Perform custom error checking here.
	public override string ErrorCheck()
	{
		// Return an error string or null if no error.
		
		return null;
	}

    private void Do()
    {
       
    }

    private void OnClick()
    {
        if (null != clickEvent)
            Fsm.Event(clickEvent);
    }

}
