//-----------------------------------------------------------------------------
// 
// Copyright (c) Crenovator Lab Corp.
//
// Author : Limiko Liu
// Data : 2013/10/28
//
//-----------------------------------------------------------------------------

using UnityEngine;
using HutongGames.PlayMaker;

[ActionCategory("UniGameKit_HUD")]
public class Detach_From_HUD_PM : FsmStateAction
{
    public FsmOwnerDefault gameObject;

	// Code that runs on entering the state.
	public override void OnEnter()
	{
        Do();
        Finish();
	}

	// Code that runs every frame.
	public override void OnUpdate()
	{
       
	}

	public override void OnFixedUpdate()
	{
		
	}

	public override void OnLateUpdate()
	{
		
	}

	// Code that runs when exiting the state.
	public override void OnExit()
	{
		
	}

	// Perform custom error checking here.
	public override string ErrorCheck()
	{
		// Return an error string or null if no error.
		
		return null;
	}

    private void Do()
    {
        var go = Fsm.GetOwnerDefaultTarget(gameObject);
        if (null == go)
            return;

        NGUITools.Destroy(go);
       
    }

}
