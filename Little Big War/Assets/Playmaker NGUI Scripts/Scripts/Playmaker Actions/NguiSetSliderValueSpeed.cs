using HutongGames.PlayMaker;

/*
 * *************************************************************************************
 * Created by: Rocket Games Mobile  (http://www.rocketgamesmobile.com), 2013-2014
 * For use in Unity 3.5, Unity 4.0+
 * 
 * Updated for NGUI v3
 * *************************************************************************************
*/

[ActionCategory("NGUI")]
[Tooltip("Sets the value of an NGUI progressbar or Slider")]
public class NguiSetSliderValueSpeed : FsmStateAction
{
    [RequiredField]
    [Tooltip("NGUI slider or progressbar to update")]
    public FsmOwnerDefault NguiSlider;

    [RequiredField]
    [Tooltip("The new value to assign to the slider progressbar")]
    public FsmFloat value;

	[RequiredField]
	[Tooltip("The new value to assign to the slider progressbar")]
	public FsmFloat Speed=0.03f;

    [UIHint(UIHint.Variable)]
    [Tooltip("Save the value to a variable")]
    public FsmFloat saveValue;

	
	[Tooltip("When true, runs on every frame")]
    private bool isWork;

	[UIHint(UIHint.Variable)]
	[Tooltip("Save the value to a variable")]
	private  FsmFloat storeValue;

    public override void Reset()
    {
        NguiSlider = null;
        value = null;
        saveValue = null;
		isWork = false;
		storeValue=1;
    }

    public override void OnEnter()
    {
		isWork = true;
		DoSetSliderValue();
    }

	public override void OnUpdate()
    {
        DoSetSliderValue();
    }

    private void DoSetSliderValue()
    {
		if(!isWork)
			return;
        // exit if objects are null
        if ((NguiSlider == null) || (value == null))
            return;

        // get the object as a progressbar
        UISlider NguiSlide = Fsm.GetOwnerDefaultTarget(NguiSlider).GetComponent<UISlider>();

        // exit if no slider
        if (NguiSlide == null)
            return;

        // set value
		if(storeValue.Value>value.Value)
		{
			storeValue.Value= storeValue.Value-Speed.Value;
			NguiSlide.value =storeValue.Value;
		}
       
        // save value
		if(storeValue.Value<=value.Value)
		{
			NguiSlide.value=value.Value;
			if (saveValue != null)
				saveValue.Value = value.Value;
			storeValue.Value=value.Value;
			isWork=false;
			Finish ();
		}
     
    }
}